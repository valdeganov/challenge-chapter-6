'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('user_games', [{
        nama: 'Dwyna Agusti',
        email: 'dwyna.gusti@gmail.com',
        password: '123456',
        createdAt: new Date(),
        updatedAt: new Date()
      }], {});
  },

  down: (queryInterface, Sequelize) => {
     return queryInterface.bulkDelete('user_games', null, {});
  }
};
