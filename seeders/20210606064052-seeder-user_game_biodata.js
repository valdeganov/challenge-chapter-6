'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('user_game_biodata', [{
      user_game_id: 1,
      alamat: 'jalan kunang',
      jenis_kelamin: 'laki-laki',
      umur: 25,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
},

down: (queryInterface, Sequelize) => {
  return queryInterface.bulkDelete('user_game_biodata', null, {});
}
};
