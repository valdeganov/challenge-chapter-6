'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('user_game_histories', [{
      user_game_id: 1,
      level: 22,
      point: 3,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
},

down: (queryInterface, Sequelize) => {
  return queryInterface.bulkDelete('user_game_histories', null, {});
}
};
