const express = require('express');
const app = express();

const { user_game, user_game_biodata, user_game_history } = require('./models');

app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));


app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'))

app.get('/login', (req, res) => {
    res.render('login');
})

app.post('/login', (req, res) => {
    const email = req.body.email;
    const password = req.body.password;

    user_game.findAll({ 
        where: {
        email: email, 
        password: password
        }
    }).then(userGame => {
        if(userGame.length === 0){
            res.redirect(301,'login');
        }
        res.redirect(301, '/dashboard');
    })
})

app.get('/dashboard', (req, res) => {
    user_game.findAll().then(userGame => {
        res.render('dashboard', { userGame });
    })
})

app.get('/user/add', (req, res) => {
    res.render('userAddView')
})

app.post('/user/save', (req, res) => {
    user_game.create({
        nama: req.body.nama,
        email: req.body.email
    }).then(userGame => {
        user_game_biodata.create({
            user_game_id: userGame.id,
            alamat: req.body.alamat,
            jenis_kelamin: req.body.jenis_kelamin,
            umur: req.body.umur
        }).then(userGame => {
            user_game_history.create({
                user_game_id: userGame.id,
                level: req.body.level,
                point: req.body.point
            }).then(biodata => res.redirect(301,'/dashboard'));
        })
    })
})

app.get('/user/delete/:id', (req, res) => {
    const userId = req.params.id;

    user_game_biodata.destroy({
        where: {
            user_game_id: userId
        }
    }).then(biodata => {
        user_game_history.destroy({
            where: {
                id: userId
            }
        })
    }).then(biodata => {
            user_game.destroy({
                where: {
                    id: userId
                }
            }).then(user => {
            res.redirect(301,'/dashboard')
        })
    })
})

app.get('/user/update/:id', (req, res) => {
    const userId = req.params.id;
    
    user_game.findOne({
        where: {
            id: userId
        }
    }).then(user => {
        user_game_biodata.findOne({
          where: {
            user_game_id: user.id
          }
        }).then(user1 => {
            user_game_history.findOne({
              where: {
                user_game_id: user.id
              }
            }).then(biodata => {
                res.render('update', { user, user1, biodata })
            })
        })
    })
})

app.post('/user/update/:id', (req, res) => {
    const userId = req.params.id;
    
    user_game.update({
        nama: req.body.nama,
        email: req.body.email
      }, {
        where: {
          id: userId
        }
      }).then(user => {
        user_game_biodata.update({
          alamat: req.body.alamat,
          jenis_kelamin: req.body.jenis_kelamin,
          umur: req.body.umur
        }, {
          where: {
            user_game_id: userId
          }
        }).then(user => {
            user_game_history.update({
              level: req.body.level,
              point: req.body.point
            }, {
              where: {
                user_game_id: userId
              }
            }).then(biodata => {
          res.redirect(301, '/dashboard')
        })
      })
    })
})

app.get('/api/user', (req, res) => {
    user_game.findAll().then(userGame => {
      res.status(200).json(userGame)
    })
  });
  
  app.post('/api/user/save', (req, res) => {
    user_game.create({
        nama: req.body.nama,
        email: req.body.email
      }).then(user => {
        user_game_biodata.create({
          user_game_id: user.id,
          alamat: req.body.alamat,
          jenis_kelamin: req.body.jenis_kelamin
        }).then(user => {
          user_game_history.create({
            user_game_id: user.id,
            level: req.body.level,
            point: req.body.point
          }).then(biodata => {
          res.status(200).json({ message: "berhasil create data" })
        })
      })
    })
  })

  app.get('api/user/delete/:id', (req, res) => {
    const userId = req.params.id;

    user_game_biodata.destroy({
        where: {
            user_game_id: userId
        }
    }).then(biodata => {
        user_game_history.destroy({
            where: {
                id: userId
            }
        })
    }).then(biodata => {
            user_game.destroy({
                where: {
                    id: userId
                }
            }).then(user => {
                res.status(301).json({ message: "berhasil delete data" })
        })
    })
})


  app.get('/api/user/update/:id', (req, res) => {
    const userId = req.params.id;
  
    user_game.findOne({
      where: {
        id: userId,
      }
    }).then(user1 => {
      user_game_biodata.findOne({
        where: {
          user_game_id: userId
        }
      }).then(user2 => {
        user_game_history.findOne({
          where: {
            user_game_id: userId
          }
        }).then(user3 => {
            res.status(200).json({user1, user2, user3})
      })
    })
  })
})




  app.post('/api/user/update/:id', (req, res) => {
    const userId = req.params.id;
  
    user_game.update({
      nama: req.body.nama,
      email: req.body.email
    }, {
      where: {
        id: userId
      }
    }).then(user => {
      user_game_biodata.update({
        alamat: req.body.alamat,
        jenis_kelamin: req.body.jenis_kelamin
      }, {
        where: {
          user_game_id: userId
        }
      }).then(user => {
        user_game_history.update({
          level: req.body.level,
          point: req.body.point
        }, {
          where: {
            user_game_id: userId
          }
        }).then(biodata => {
        res.status(200).json({ message: "berhasil update data" })
      })
    })
  })
})
    

app.listen(3000, () => console.log('Server berjalan pada port 3000'));